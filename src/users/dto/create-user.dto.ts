import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, IsString } from 'class-validator';

export class CreateUserDto {
  @ApiProperty({
    default: 'lequochung2001@gmail.com',
  })
  @IsEmail()
  email: string;

  @ApiProperty({
    default: 'password',
  })
  @IsNotEmpty()
  password: string;

  @ApiProperty({
    default: 'Hung Randy',
  })
  @IsString()
  name: string;
}
