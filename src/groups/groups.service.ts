import { Injectable } from '@nestjs/common';
import { CreateGroupDto } from './dto/create-group.dto';
import { UpdateGroupDto } from './dto/update-group.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Group } from './entities/group.entity';
import { Repository } from 'typeorm';
import { JwtPayloadType } from 'src/auth/strategies/types/jwt-payload.type';
import { QueryGroupDto } from './dto/query-group.dto';

@Injectable()
export class GroupsService {
  constructor(
    @InjectRepository(Group)
    private groupRepository: Repository<Group>,
  ) {}
  async create(
    createGroupDto: CreateGroupDto,
    userJwtPayload: JwtPayloadType,
  ): Promise<Group | null> {
    const newGroup = this.groupRepository.create(createGroupDto);
    newGroup.owner = userJwtPayload.id;
    return await this.groupRepository.save(newGroup);
  }

  async findAll(query: QueryGroupDto): Promise<Group[]> {
    const { search, limit, page } = query;
    const pageNumber = parseInt(page, 10) || 1;
    const limitNumber = parseInt(limit, 10) || 10;
    const offset = (pageNumber - 1) * limitNumber;
    const queryBuilder = this.groupRepository.createQueryBuilder('groups');
    if (search) {
      queryBuilder.where('groups.name like :search', { search: `%${search}%` });
      queryBuilder.where('groups.description like :description', {
        description: `%${search}%`,
      });
    }

    return queryBuilder.skip(offset).take(limitNumber).getMany();
  }

  findOne(id: number) {
    return `This action returns a #${id} group`;
  }

  update(id: number, updateGroupDto: UpdateGroupDto) {
    return `This action updates a #${id} group`;
  }

  remove(id: number) {
    return `This action removes a #${id} group`;
  }
}
