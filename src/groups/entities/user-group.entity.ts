import { Exclude } from 'class-transformer';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity({
  name: 'user_groups',
})
export class UserGroup {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'integer' })
  @Exclude({ toPlainOnly: true })
  groupId: number;

  @Column({ type: 'integer' })
  @Exclude({ toPlainOnly: true })
  userId: number;
}
