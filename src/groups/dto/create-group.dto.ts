import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';

export class CreateGroupDto {
  @ApiProperty({
    default: 'Name Group',
  })
  @IsString()
  name: string;

  @ApiProperty({
    default: 'description group',
  })
  @IsString()
  description: string;
}
