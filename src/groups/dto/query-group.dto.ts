import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';
import { QueryDto } from 'src/common/dto/query.dto';

export class QueryGroupDto extends QueryDto {
  @ApiProperty({
    default: 'Name Group',
  })
  @IsString()
  search: string;
}
