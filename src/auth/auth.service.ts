import { Injectable, NotFoundException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from 'src/users/users.service';
import { LoginDto } from './dto/login.dto';
import * as bcrypt from 'bcrypt';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
    private configService: ConfigService,
  ) {}
  async login(loginDto: LoginDto) {
    const { password, email } = loginDto;
    const user = await this.usersService.findByEmail(email);
    if (!user) {
      throw new NotFoundException(`User with ID not found`);
    }
    if (!bcrypt.compare(password, user.password)) {
      throw new NotFoundException(`Password not found`);
    }
    const tokenExpiresIn = this.configService.get<number>('JWT_EXPIRES', 86400);
    return {
      access_token: await this.jwtService.signAsync(
        {
          id: user.id,
          email: email,
        },
        {
          secret: this.configService.get<string>('JWT_SECRET', 'jwt_secret'),
          expiresIn: tokenExpiresIn,
        },
      ),
    };
  }
}
