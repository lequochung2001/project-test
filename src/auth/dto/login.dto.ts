import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, IsString } from 'class-validator';

export class LoginDto {
  @ApiProperty({
    default: 'lequochung2001@gmail.com',
  })
  @IsEmail()
  email: string;

  @ApiProperty({
    default: 'password',
  })
  @IsNotEmpty()
  @IsString()
  password: string;
}
